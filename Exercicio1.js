let array = []
function criaArray(num){
    
    for (let index = 0; index < num; index++) {
        array.push(Math.floor( Math.random()*(100-10))+10);
    }
    return array
}
function ordena(array){
    let array_ordenado = array
    for (let i = 0; i < array_ordenado.length; i++) {
		for (let j = 0; j < array_ordenado.length; j++) {
			if (array_ordenado[j] > array_ordenado[j + 1]) {
				let temp = array_ordenado[j];
				array_ordenado[j] = array_ordenado[j + 1];
				array_ordenado[j + 1] = temp;
			}
		}
	}
	return array;
}
console.log("O Array criado e não ordenado: "+ criaArray(5))
console.log("O Array criado e ordenado: "+ordena(array))