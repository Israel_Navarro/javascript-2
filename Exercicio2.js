function multiplica(matriz1,matriz2){
    var matriz1NumRows = matriz1.length, matriz1NumCols = matriz1[0].length,
    matriz2NumRows = matriz2.length, matriz2NumCols = matriz2[0].length,
      m = new Array(matriz1NumRows);  // initialize array of rows
  for (var r = 0; r < matriz1NumRows; ++r) {
    m[r] = new Array(matriz2NumCols); // initialize the current row
    for (var c = 0; c < matriz2NumCols; ++c) {
      m[r][c] = 0;             // initialize the current cell
      for (var i = 0; i < matriz1NumCols; ++i) {
        m[r][c] += matriz1[r][i] * matriz2[i][c];
      }
    }
  }
  return m;
}
console.log("Caso teste 1: ")
console.log(multiplica( [ [ [2],[-1] ], [ [2],[0] ] ],[ [2,3],[-2,1] ]  ))
console.log("Caso teste 2: ")
console.log(multiplica([[4,0],[-1,-1]], [[-1,3],[2,7]]  ))